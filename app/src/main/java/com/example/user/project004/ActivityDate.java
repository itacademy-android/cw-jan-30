package com.example.user.project004;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ActivityDate extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time);

        TextView textView = findViewById(R.id.textView);
        SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy");
        String str = sdf.format(new Date(System.currentTimeMillis()));
        textView.setText(str);
    }
}
