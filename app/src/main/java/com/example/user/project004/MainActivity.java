package com.example.user.project004;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnTime, btnDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnDate = findViewById(R.id.btnDate);
        btnTime = findViewById(R.id.btnTime);

        btnTime.setOnClickListener(onClickListener);
        btnDate.setOnClickListener(onClickListener);

        Log.d("MAIN", "onCreate");
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            startActivity(intent);
            /*switch (view.getId()) {
                case R.id.btnTime:
                    Intent intentTime = new Intent("itacademy.SHOWTIME");
                    startActivity(intentTime);
                    break;
                case R.id.btnDate:
                    Intent intentDate = new Intent("itacademy.SHOWDATE");
                    startActivity(intentDate);
                    break;
            }*/
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MAIN", "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MAIN", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MAIN", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MAIN", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MAIN", "onDestroy");
    }
}
